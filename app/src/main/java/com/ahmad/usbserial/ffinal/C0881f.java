package com.ahmad.usbserial.ffinal;

import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;

import com.hoho.android.usbserial.driver.CdcAcmSerialDriver;
import com.hoho.android.usbserial.driver.FtdiSerialDriver;
import com.hoho.android.usbserial.driver.ProlificSerialDriver;
import com.hoho.android.usbserial.driver.UsbId;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;

import java.util.Iterator;
import java.util.Set;

/* renamed from: de.kai_morich.serial_usb_terminal.f */
class C0881f {
    public static final String[] f2639a;

    static {
        f2639a = new String[]{"- no driver -", "CDC", "CH340", "CP210x", "FTDI", "PL2302"};
    }

    static UsbSerialDriver m5045a(UsbManager usbManager, Set<String> set) {
        for (UsbDevice c : usbManager.getDeviceList().values()) {
            UsbSerialDriver c2 = C0881f.m5051c(c, set);
            if (c2 != null) {
                return c2;
            }
        }
        return null;
    }

    static String m5046a(UsbDevice usbDevice) {
        return usbDevice == null ? null : usbDevice.getVendorId() == UsbId.VENDOR_FTDI ? "FTDI" : usbDevice.getVendorId() == UsbId.VENDOR_ARDUINO ? "Arduino" : (usbDevice.getVendorId() == UsbId.VENDOR_VAN_OOIJEN_TECH && usbDevice.getProductId() == UsbId.VAN_OOIJEN_TECH_TEENSYDUINO_SERIAL) ? "Van Ooijen Technische Informatica" : usbDevice.getVendorId() == UsbId.VENDOR_SILABS ? "Silicon Labs" : (usbDevice.getVendorId() == UsbId.VENDOR_PROLIFIC && usbDevice.getProductId() == UsbId.PROLIFIC_PL2303) ? "Prolific" : (usbDevice.getVendorId() == UsbId.VENDOR_QINHENG && usbDevice.getProductId() == UsbId.QINHENG_HL340) ? "Qinheng" : usbDevice.getVendorId() == UsbId.VENDOR_ATMEL ? "Atmel" : usbDevice.getVendorId() == UsbId.VENDOR_LEAFLABS ? "Leaflabs" : null;
    }

    static String m5047a(UsbDevice usbDevice, Set<String> set) {
        int b = C0881f.m5049b(usbDevice, set);
        return b > 0 ? f2639a[b] : null;
    }

    static void m5048a(UsbDevice usbDevice, int i, Set<String> set) {
        Iterator it = set.iterator();
        while (it.hasNext()) {
            String[] split = ((String) it.next()).split("/");
            if (split.length == 3 && usbDevice.getVendorId() == Integer.parseInt(split[0]) && usbDevice.getProductId() == Integer.parseInt(split[1])) {
                it.remove();
            }
        }
        if (i != 0) {
            set.add(usbDevice.getVendorId() + "/" + usbDevice.getProductId() + "/" + i);
        }
    }

    static int m5049b(UsbDevice usbDevice, Set<String> set) {
        for (String split : set) {
            String[] split2 = split.split("/");
            if (split2.length == 3 && usbDevice.getVendorId() == Integer.parseInt(split2[0]) && usbDevice.getProductId() == Integer.parseInt(split2[1])) {
                try {
                    int intValue = Integer.valueOf(split2[2]).intValue();
                    if (intValue < f2639a.length) {
                        return intValue;
                    }
                } catch (NumberFormatException e) {
                }
            }
        }
        return 0;
    }

    static String m5050b(UsbDevice usbDevice) {
        return usbDevice == null ? null : usbDevice.getVendorId() == UsbId.VENDOR_FTDI ? usbDevice.getProductId() == UsbId.FTDI_FT232R ? "FT232R" : usbDevice.getProductId() == UsbId.FTDI_FT231X ? "FT231X" : null : usbDevice.getVendorId() == UsbId.VENDOR_ARDUINO ? usbDevice.getProductId() == 1 ? "Uno" : usbDevice.getProductId() == 16 ? "Mega 2560" : usbDevice.getProductId() == 59 ? "Serial Adapter" : usbDevice.getProductId() == 63 ? "Mega ADK" : usbDevice.getProductId() == 66 ? "Mega 2560 R3" : usbDevice.getProductId() == 67 ? "Uno R3" : usbDevice.getProductId() == 68 ? "Mega ADK R3" : usbDevice.getProductId() == 68 ? "Serial Adapter R3" : usbDevice.getProductId() == UsbId.ARDUINO_LEONARDO ? "Leonardo" : usbDevice.getProductId() == UsbId.ARDUINO_MICRO ? "Micro" : "???" : (usbDevice.getVendorId() == UsbId.VENDOR_VAN_OOIJEN_TECH && usbDevice.getProductId() == UsbId.VAN_OOIJEN_TECH_TEENSYDUINO_SERIAL) ? "Teensyduino" : usbDevice.getVendorId() == UsbId.VENDOR_SILABS ? "CP210x" : (usbDevice.getVendorId() == UsbId.VENDOR_PROLIFIC && usbDevice.getProductId() == UsbId.PROLIFIC_PL2303) ? "PL2303" : (usbDevice.getVendorId() == UsbId.VENDOR_QINHENG && usbDevice.getProductId() == UsbId.QINHENG_HL340) ? "CH340" : usbDevice.getVendorId() == UsbId.VENDOR_ATMEL ? "Lufa CDC Demo" : usbDevice.getVendorId() == UsbId.VENDOR_LEAFLABS ? "Maple" : null;
    }

    private static UsbSerialDriver m5051c(UsbDevice usbDevice, Set<String> set) {
        switch (C0881f.m5049b(usbDevice, set)) {
            case UsbId.ARDUINO_UNO /*1*/:
                return new CdcAcmSerialDriver(usbDevice);
//            case FtdiSerialPort.USB_RECIP_ENDPOINT /*2*/:
//                return new Ch34xSerialDriver(usbDevice);
//            case FtdiSerialPort.USB_RECIP_OTHER /*3*/:
//                return new Cp21xxSerialDriver(usbDevice);
            case UsbId.LEAFLABS_MAPLE /*4*/:
                return new FtdiSerialDriver(usbDevice);
            case UsbSerialPort.DATABITS_5 /*5*/:
                return new ProlificSerialDriver(usbDevice);
            default:
                return null;
        }
    }

    static String m5052c(UsbDevice usbDevice) {
        return usbDevice == null ? null : usbDevice.getVendorId() == UsbId.VENDOR_FTDI ? "FTDI" : usbDevice.getVendorId() == UsbId.VENDOR_ARDUINO ? "CDC" : (usbDevice.getVendorId() == UsbId.VENDOR_VAN_OOIJEN_TECH && usbDevice.getProductId() == UsbId.VAN_OOIJEN_TECH_TEENSYDUINO_SERIAL) ? "CDC" : usbDevice.getVendorId() == UsbId.VENDOR_SILABS ? "CP210x" : (usbDevice.getVendorId() == UsbId.VENDOR_PROLIFIC && usbDevice.getProductId() == UsbId.PROLIFIC_PL2303) ? "PL2303" : (usbDevice.getVendorId() == UsbId.VENDOR_QINHENG && usbDevice.getProductId() == UsbId.QINHENG_HL340) ? "CH34x" : usbDevice.getVendorId() == UsbId.VENDOR_ATMEL ? "CDC" : usbDevice.getVendorId() == UsbId.VENDOR_LEAFLABS ? "CDC" : null;
    }
}
